# Design #

## Iteration 1

Create a Object Page component for the UI5 application. This will work as a baseline for other improvements

![concepts](concepts.jpg)
![layout](layout.jpg)


1. Create object page
2. Include all content from the CV. Follow the SAP Design guidelines and use Section and Subsection controls to organise the content within the object page


