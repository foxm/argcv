# Code #

A first version of the code implemented through a simple Controller and View. 

No Model was implemented. Instead the data was embedded in the view. This is, of course, something that will be changed later.

Implementation was modelled after the Object Page guidelines in the experience.sap.com website, and contained in a Shell control.
