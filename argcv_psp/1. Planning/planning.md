# Requirement #

Set a baseline for the implementation of the ARGCV personal project.

This could be done by implementing a basic UI5 Object Page with a header and content areas.

The header should have the contact details

The content should have the following sections and subsections (not exhaustive)

1. Work experience
    1. Experience for last job
    2. Experience for second to last job
    3. ...
2. Education
    1. Master's Details
    2. Bachelor Details
3. Skills
    1. List of skills from CV
4. Projects
    1. Mention of personal projects developed during my professional career.
5. Programming Languages
    1. Proficient languages
    2. Basic knowledge
6. Natural Languages
    1. Proficient with English and Spanish
    2. Knowledge of Italian and French

As part of the requirement for this project, the Design and Implementation process should be documented.

For this, the design can be complemented by an iterative approach to Design+Implementation.

The Implementation will be documented by following the PSP0 principles

