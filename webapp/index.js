sap.ui.require([
  "sap/m/Shell",
  "sap/m/App",
  "sap/m/Page",
  "sap/ui/core/ComponentContainer"
], function (Shell, App, Page, ComponentContainer) {
	"use strict";


  new Shell({
    app : new App({
      pages : [
        new Page({
          title : "ARGCV",
          enableScrolling : false,
          content : [
            new ComponentContainer({
              name : "arguapacha.argcv",
              height : "100%",
              settings : {
                id : "argcv"
              }
            })
          ]
        })
      ]
    })
  }).placeAt("content");
});

