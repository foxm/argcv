sap.ui.define([
  "sap/ui/core/UIComponent",
  "sap/ui/model/json/JSONModel",
  "sap/ui/model/resource/ResourceModel"
], function (UIComponent, JSONModel, ResourceModel) {
  "use strict";
  return UIComponent.extend("arguapacha.argcv.Component", {
    metadata : {
      manifest : "json"
    },
    init : function () {
      UIComponent.prototype.init.apply(this, arguments);
      // Data Model
      var oData = {};
      var oModel = new JSONModel(oData);
      this.setModel(oModel);

      // i18n Model
      var i18nModel = new ResourceModel({
        bundleName : "arguapacha.argcv.i18n.i18n"
      });
      this.setModel(i18nModel, "i18n");
    }
  });
});
