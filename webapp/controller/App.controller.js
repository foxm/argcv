sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/m/MessageToast",
], function (Controller, MessageToast) {
  "use strict";
  return Controller.extend("arguapacha.argcv.controller.App", {
    _getVal : function(evt) {
      return sap.ui.getCore().byId(evt.getParameter('id')).getText();
    },

    handleEmailPress : function (evt) {
      sap.m.URLHelper.triggerEmail(this._getVal(evt), "Information request")
    }
  });
});
